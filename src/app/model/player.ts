export class Player {
  id: number;
  firstName: string;
  lastName: string;
  email: string;
  height: number;
  country: string;
}
